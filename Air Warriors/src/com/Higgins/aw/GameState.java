package com.Higgins.aw;

import com.Higgins.aw.GameState;

public enum GameState {

	LOBBY, IN_GAME, RESTART;
	
	public static GameState state;
	
	public static boolean getState(GameState state) {
		return GameState.state == state;
		
	}
	
	public static void setState(GameState state) {
		GameState.state = state;
	}
}
