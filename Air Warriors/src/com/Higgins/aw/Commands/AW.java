package com.Higgins.aw.Commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.Higgins.aw.Main;

public class AW implements CommandExecutor {

	File file = new File("plugins/AirWarriors", "SpawnLocations.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("aw")) {

			if (!(sender instanceof Player)) {

				sender.sendMessage("�cYou must be a player to use this command");

			} else {

				Player p = (Player) sender;

				double x = p.getLocation().getX();
				double y = p.getLocation().getY();
				double z = p.getLocation().getZ();
				float yaw = p.getLocation().getYaw();
				float pitch = p.getLocation().getPitch();
				String direction = p.getLocation().getDirection().toString();

				if (p.isOp()) {

					if (args.length == 0) {

						p.sendMessage("�bCommands:");
						p.sendMessage("�e/aw setlobby");
						p.sendMessage("�e/aw setspawn <spawnid>");

					} else {

						if (args.length == 1) {

							if (args[0].equalsIgnoreCase("setlobby")) {

								cfg.set("Lobby.x", x);
								cfg.set("Lobby.y", y);
								cfg.set("Lobby.z", z);
								cfg.set("Lobby.yaw", yaw);
								cfg.set("Lobby.pitch", pitch);

								try {
									cfg.save(file);
									p.sendMessage(Main.prefix
											+ "Successfully set the lobby!");
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							} else {

								p.sendMessage("�bCommands:");
								p.sendMessage("�e/aw setlobby");
								p.sendMessage("�e/aw setspawn <spawnid>");
							}

						} else {

							if (args.length == 2) {

								if (args[0].equalsIgnoreCase("setspawn")) {

									cfg.set(args[1] + ".x", x);
									cfg.set(args[1] + ".y", y);
									cfg.set(args[1] + ".z", z);
									cfg.set(args[1] + ".yaw", yaw);
									cfg.set(args[1] + ".pitch",
											pitch);
									cfg.set(args[1] + ".direction",
											direction);

									try {
										cfg.save(file);
										p.sendMessage(Main.prefix
												+ "Successfully set spawn #"
												+ args[1]);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								} else {

									p.sendMessage("�bCommands:");
									p.sendMessage("�e/aw setlobby");
									p.sendMessage("�e/aw setspawn <spawnid>");

								}
							}
						}
					}

				} else {

					p.sendMessage("�cYou do not permission to execute this command!");
				}
			}
		}
		return true;
	}
}
