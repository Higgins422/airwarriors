package com.Higgins.aw.Countdowns;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.Higgins.aw.BossBar;
import com.Higgins.aw.GameState;
import com.Higgins.aw.Main;
import com.Higgins.aw.Utils;

public class GameCountdown extends BukkitRunnable {

	public static int time;

	@Override
	public void run() {
		if(time == 0) {
			
			GameState.setState(GameState.RESTART);
			
			Player winner = null;
			int winningScore = 0;
			
			for (Map.Entry<Player, Integer> entry : Main.kills.entrySet()) {
				
				if (entry.getValue() > winningScore) {
					
					winningScore = entry.getValue();
					winner = entry.getKey();
				}
			}
			
			for(Player pl : Bukkit.getOnlinePlayers()) {
				
				pl.playSound(pl.getLocation(), Sound.WITHER_SPAWN, 1, 1);
				Utils.sendTitle(pl, winner.getName(), 1, 4 , 2, ChatColor.GREEN);
				Utils.sendSubtitle(pl, "has won the game with �6" + winningScore + " �epoints!", 1, 4, 2, ChatColor.YELLOW);
			}
			
			return;
		}
		
		if(time <= 5) {
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �6" + String.valueOf(time) + "�r seconds.");
		}
		
		else if(time == 10) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �610 �eseconds.");
		}
		
		else if(time == 15) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �615 �eseconds.");
		}
		
		else if(time == 30) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �630 �eseconds.");
		}
		
		else if(time == 60) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �660 �eseconds.");
		}
		
		else if(time == 120) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �62 �eminutes.");
		}
		
		else if(time == 180) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �63 �eminutes.");
		}
		
		else if(time == 240) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �64 �eminutes.");
		}
		
		else if(time == 300) {
			
			Bukkit.broadcastMessage(Main.prefix + "Game ending in �65 �eminutes.");
		}
		
		time -= 1;
		
		int startTime = GameCountdown.time;
		int remainder = startTime % 3600;
		int minutes = remainder / 60;
		int seconds = remainder % 60;
		String min = (minutes < 10 ? "0" : "") + minutes;
		String sec = (seconds < 10 ? "0" : "") + seconds;
		String gameTime = min + ":" + sec;
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			
			BossBar.displayText(pl, "�c�lGAME ENDING IN �e�l" + gameTime, 1);
		}
	}
}
