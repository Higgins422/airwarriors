package com.Higgins.aw.Countdowns;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.Higgins.aw.AirWarriors;
import com.Higgins.aw.BossBar;
import com.Higgins.aw.GameState;
import com.Higgins.aw.Main;
import com.Higgins.aw.Utils;
import com.Higgins.aw.Scoreboards.GameBoard;

public class LobbyCountdown extends BukkitRunnable {

	public static int time;
	
	@Override
	public void run() {
		if(time == 0) {
			if(Bukkit.getOnlinePlayers().size() == 0) {
				
				Bukkit.broadcastMessage(Main.prefix + "Waiting for more players. Restarting countdown...");
				Main.getInstance().stopLobbyCountdown();
				Main.getInstance().startLobbyCountdown();
				
				return;
			}
			
			GameState.setState(GameState.IN_GAME);
			Main.getInstance().stopLobbyCountdown();
			Main.getInstance().startGameCountdown();
			
			for(final Player pl : Bukkit.getOnlinePlayers()) {
				
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
					
					public void run() {
					
						pl.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
						
						pl.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
						pl.getInventory().addItem(new ItemStack(Material.FLINT_AND_STEEL));
						
						pl.setLevel(30);
						
						AirWarriors.teleportSpawn("AirWarriors", pl);
						
						GameBoard.makeScoreboard();
						
						pl.setScoreboard(GameBoard.board);
					}
					
				}, 20L);
			}
			
			return;
		}
		
		if(time % 10 == 0 || time <= 5) {
			Bukkit.broadcastMessage(Main.prefix + "Game starting in " + String.valueOf(time) + " seconds.");
			for(Player pl : Bukkit.getOnlinePlayers()) {
				
				pl.playSound(pl.getLocation(), Sound.CLICK , 1, 1);
				
			}
		}
		
		time -= 1;
		
		int startTime = LobbyCountdown.time;
		int remainder = startTime % 3600;
		int minutes = remainder / 60;
		int seconds = remainder % 60;
		String min = (minutes < 10 ? "0" : "") + minutes;
		String sec = (seconds < 10 ? "0" : "") + seconds;
		String lobbyTime = min + ":" + sec;
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			
			pl.setLevel(time);
			BossBar.displayText(pl, "�a�lGAME STARTING IN �e�l" + lobbyTime, 1);
		}
	}
}
