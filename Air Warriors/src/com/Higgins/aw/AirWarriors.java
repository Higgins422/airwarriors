package com.Higgins.aw;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AirWarriors {
	
	static File file = new File("plugins/AirWarriors", "SpawnLocations.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public static ArrayList<Player> spawned = new ArrayList<>();
	
	public static void teleportSpawn(String world, Player p) {
		
		if(!spawned.contains(p)) {
			
			spawned.add(p);
			
			double x = cfg.getDouble(Main.playerNumber.get(p) + ".x");
			double y = cfg.getDouble(Main.playerNumber.get(p) + ".y");
			double z = cfg.getDouble(Main.playerNumber.get(p) + ".z");
			float yaw = cfg.getInt(Main.playerNumber.get(p) + "yaw");
			float pitch = cfg.getInt(Main.playerNumber.get(p) + "pitch");
			
			Location loc = new Location(Bukkit.getWorld("AirWarriors"), x, y, z);
			
			loc.setYaw(yaw);
			loc.setPitch(pitch);
			
			p.teleport(loc);
		}
	}
	
	public static boolean isFull() {
		
		if(Bukkit.getOnlinePlayers().size() == 12) {
			
			return true;
		}
		return false;
	}
	
	public static void makeSpectator(Player p) {
		
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		
		p.getInventory().clear();
		
		ItemStack spec = new ItemStack(Material.COMPASS);
		ItemMeta meta = spec.getItemMeta();
		meta.setDisplayName("§aSpectate §7(Coming Soon)");
		spec.setItemMeta(meta);
		
		p.getInventory().setItem(0, spec);
		p.getInventory().setHeldItemSlot(0);
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			
			pl.hidePlayer(p);
		}
		
		for(Player specs : Main.spectator) {
			
			specs.hidePlayer(p);
		}
		
		p.setAllowFlight(true);
		p.setFlying(true);
		
		p.sendMessage(Main.prefix + "You are now spectating");
	}
}
