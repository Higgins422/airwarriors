package com.Higgins.aw;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.Higgins.aw.Commands.AW;
import com.Higgins.aw.Countdowns.GameCountdown;
import com.Higgins.aw.Countdowns.LobbyCountdown;
import com.Higgins.aw.Countdowns.RestartCountdown;
import com.Higgins.aw.Game.GameUtils;
import com.Higgins.aw.Game.Jetpack;
import com.Higgins.aw.Game.Refuel;
import com.Higgins.aw.Game.Player.PlayerDamage;
import com.Higgins.aw.Game.Player.PlayerDeath;
import com.Higgins.aw.Game.Player.PlayerJoin;
import com.Higgins.aw.Game.Player.PlayerLogin;
import com.Higgins.aw.Game.Player.PlayerQuit;
import com.Higgins.aw.Game.Player.PlayerRespawn;
import com.Higgins.aw.Scoreboards.LobbyBoard;

public class Main extends JavaPlugin {

	public static ArrayList<Player> spectator = new ArrayList<>();
	public static HashMap<Player, Integer> playerNumber = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	
	public static String prefix = "�aAW�7>> �e";
	
	public static int spawn = 0;
	
	public static Main instance;
	
	public static int startLobbyCountdownId;
    public static int startGameCountdownId;
    public static int startRestartCountdownId;
	
	public void onEnable() {
		
		instance = this;
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		
		pm.registerEvents(new PlayerJoin(), this);
		pm.registerEvents(new PlayerLogin(), this);
		pm.registerEvents(new PlayerQuit(), this);
		pm.registerEvents(new PlayerDamage(), this);
		pm.registerEvents(new PlayerDeath(), this);
		pm.registerEvents(new PlayerRespawn(), this);
		pm.registerEvents(new GameUtils(), this);
		pm.registerEvents(new Jetpack(), this);
		pm.registerEvents(new Refuel(), this);
		
		this.getCommand("aw").setExecutor(new AW());
		
		GameState.setState(GameState.LOBBY);
		
		startLobbyCountdown();
		
		LobbyBoard.makeScoreboard();
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	public void startLobbyCountdown() {
		LobbyCountdown.time = 60;
		startLobbyCountdownId = getServer().getScheduler().scheduleSyncRepeatingTask(this, new LobbyCountdown(), 20l, 20l);
	}
	
	public void stopLobbyCountdown() {
		getServer().getScheduler().cancelTask(startLobbyCountdownId);
	}
	public void startGameCountdown() {
		GameCountdown.time = 1200;
		startGameCountdownId = getServer().getScheduler().scheduleSyncRepeatingTask(this, new GameCountdown(), 20l, 20l);
	}
	
	public void stopGameCountdown() {
		getServer().getScheduler().cancelTask(startGameCountdownId);
	}
	
	public void startRestartCountdown() {
		RestartCountdown.time = 8;
		startRestartCountdownId = getServer().getScheduler().scheduleSyncRepeatingTask(this, new RestartCountdown(), 20l, 20l);
	}
}
