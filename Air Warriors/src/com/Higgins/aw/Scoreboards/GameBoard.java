package com.Higgins.aw.Scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class GameBoard {

	public static ScoreboardManager manager = Bukkit.getScoreboardManager();
	public static Scoreboard board = manager.getNewScoreboard();

	public static int task;

	public static void makeScoreboard() {

		Objective obj = board.registerNewObjective("game1", "dummy");

		obj.setDisplayName("�a�lAir Warriors");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			
			board.getObjective(DisplaySlot.SIDEBAR).getScore(pl.getName()).setScore(0);
		}
	}
}
