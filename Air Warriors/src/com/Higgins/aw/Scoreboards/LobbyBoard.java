package com.Higgins.aw.Scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class LobbyBoard {

	public static ScoreboardManager manager = Bukkit.getScoreboardManager();
	public static Scoreboard board = manager.getNewScoreboard();

	public static int task;

	public static void makeScoreboard() {

		Objective obj = board.registerNewObjective("lobby1", "dummy");

		obj.setDisplayName("�a�lAir Warriors");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);

		Score players = obj.getScore("�e�lPlayers");
		players.setScore(-1);

		Score amount = obj.getScore("�7" + Bukkit.getOnlinePlayers().size() + "/12");
		amount.setScore(-2);

		Score space1 = obj.getScore("�1");
		space1.setScore(-3);

		Score status = obj.getScore("�e�lStatus:");
		status.setScore(-4);

		Score state = obj.getScore("�7Lobby");
		state.setScore(-5);

	}

	public static void updateScoreboard(Player p) {

		Scoreboard b = p.getScoreboard();

		Objective obj = b.getObjective("lobby1");

		for (String scores : b.getEntries()) {

			b.resetScores(scores);
		}

		Score players = obj.getScore("�e�lPlayers");
		players.setScore(-1);

		Score amount = obj.getScore("�7" + Bukkit.getOnlinePlayers().size() + "/12");
		amount.setScore(-2);

		Score space1 = obj.getScore("�1");
		space1.setScore(-3);

		Score status = obj.getScore("�e�lStatus:");
		status.setScore(-4);

		Score state = obj.getScore("�7Lobby");
		state.setScore(-5);

		p.setScoreboard(b);
	}
}
