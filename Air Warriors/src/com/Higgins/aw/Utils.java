package com.Higgins.aw;

import java.lang.reflect.Field;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Utils {
	//show_text
	//suggest_command
	//run_command
	//open_url
	public static void sendClickableMessage(Player p, String message, String click, String event, 
			String result, String value, String event2, String action, String result2) {
        
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\":\"" + message + "\",\"extra\":[{\"text\":\"" 
		+ click + "\",\"" + event + "\":{\"action\":\"show_text\",\"value\":\"" + value +"\"},\"" 
				+ event2 + "\":{\"action\":\"" + action + "\",\"value\":\"" + result2 +"\"}}]}");
        PacketPlayOutChat packet = new PacketPlayOutChat(cbc);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}
	
	public static void sendActionBar(Player player, String message){
		
		CraftPlayer p = (CraftPlayer) player;
        IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
        PacketPlayOutChat packet = new PacketPlayOutChat(cbc, (byte) 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
	
	public static void sendTitle(Player p, String text, int fadeIn, int showTime, int fadeOut, ChatColor color) {
		
		IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + text + "\",color:" + color.name().toLowerCase() + "}");
		
		PacketPlayOutTitle title = new PacketPlayOutTitle(EnumTitleAction.TITLE, chatTitle);
		PacketPlayOutTitle length = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, fadeIn, showTime ,fadeOut);
   
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
    }
	
	public static void sendSubtitle(Player p, String text, int fadeIn, int showTime, int fadeOut, ChatColor color) {
		
		IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + text + "\",color:" + color.name().toLowerCase() + "}");
		
		PacketPlayOutTitle title = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, chatTitle);
		PacketPlayOutTitle length = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, fadeIn, showTime ,fadeOut);
   
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
	
	}
	
	public static void setTab(Player p, String topText, String bottomText) {
		
	CraftPlayer craftplayer = (CraftPlayer) p;
    PlayerConnection connection = craftplayer.getHandle().playerConnection;
    IChatBaseComponent header = ChatSerializer.a("{\"text\": \"" + topText +"\"}");
    IChatBaseComponent footer = ChatSerializer.a("{\"text\": \"" + bottomText +"\"}");
    PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
 
    try {
        Field headerField = packet.getClass().getDeclaredField("a");
        headerField.setAccessible(true);
        headerField.set(packet, header);
        headerField.setAccessible(!headerField.isAccessible());
     
        Field footerField = packet.getClass().getDeclaredField("b");
        footerField.setAccessible(true);
        footerField.set(packet, footer);
        footerField.setAccessible(!footerField.isAccessible());
    } catch (Exception e) {
        e.printStackTrace();
    }
   
    connection.sendPacket(packet);
	}
}
