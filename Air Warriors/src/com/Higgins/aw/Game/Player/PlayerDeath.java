package com.Higgins.aw.Game.Player;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scoreboard.DisplaySlot;

import com.Higgins.aw.AirWarriors;
import com.Higgins.aw.Main;
import com.Higgins.aw.Scoreboards.GameBoard;

public class PlayerDeath implements Listener {

	@EventHandler
	public void on(PlayerDeathEvent e) throws ReflectiveOperationException {
		Player p = (Player) e.getEntity();
		
		e.getDrops().clear();
		
		if(p.getKiller() instanceof Player) {
			
			Player k = p.getKiller();
			
			int kills = Main.kills.get(k);
			
			kills ++;
			
			Main.kills.put(k, kills);
			
			GameBoard.board.getObjective(DisplaySlot.SIDEBAR).getScore(k.getName()).setScore(kills);
			
			int fuel = p.getLevel() + 10;
			
			p.setLevel(fuel);
			
			k.playSound(k.getLocation(), Sound.LEVEL_UP, 1, 1);
		}
	}
}
