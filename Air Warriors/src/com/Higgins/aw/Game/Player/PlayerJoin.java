package com.Higgins.aw.Game.Player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.Higgins.aw.AirWarriors;
import com.Higgins.aw.GameState;
import com.Higgins.aw.Main;
import com.Higgins.aw.Utils;
import com.Higgins.aw.Scoreboards.LobbyBoard;

public class PlayerJoin implements Listener {

	@EventHandler
	public void on(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		p.getInventory().clear();
		
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		
		if(GameState.getState(GameState.LOBBY)) {
			
			Main.playerNumber.put(p, Bukkit.getOnlinePlayers().size());
			
			e.setJoinMessage(Main.prefix + p.getName() + " has joined the game (" + Bukkit.getOnlinePlayers().size() + "/12)");
			
			p.setScoreboard(LobbyBoard.board);
			
			LobbyBoard.updateScoreboard(p);
			
			Utils.sendTitle(p, "�b�lAir Warriors", 2, 4, 1, ChatColor.AQUA);
		
		} else {
			
			e.setJoinMessage(null);
			
			AirWarriors.makeSpectator(p);
		}
	}
}
