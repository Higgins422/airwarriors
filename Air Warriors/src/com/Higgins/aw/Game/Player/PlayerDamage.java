package com.Higgins.aw.Game.Player;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class PlayerDamage implements Listener {

	@EventHandler
	public void on(EntityDamageByEntityEvent e) {
		Entity ent = e.getEntity();
		
		if(ent instanceof Player) {
			
			Player p = (Player) ent;
			
			if(!(e.getCause() == DamageCause.ENTITY_ATTACK)) {
				
				e.setDamage(20);
			}
		}
	}
}
