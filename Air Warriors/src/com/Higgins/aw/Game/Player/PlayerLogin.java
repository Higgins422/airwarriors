package com.Higgins.aw.Game.Player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.Higgins.aw.AirWarriors;
import com.Higgins.aw.Main;

public class PlayerLogin implements Listener {

	@EventHandler
	public void on(PlayerLoginEvent e) {
		
		if(AirWarriors.isFull() == true) {
			
			e.disallow(Result.KICK_FULL, Main.prefix + "This game is full!");
		}
	}
}
