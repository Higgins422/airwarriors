package com.Higgins.aw.Game.Player;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.Higgins.aw.Main;

public class PlayerRespawn implements Listener {

	static File file = new File("plugins/AirWarriors", "SpawnLocations.yml");
	static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	@EventHandler
	public void on(PlayerRespawnEvent e) {
		final Player p = e.getPlayer();
		
		p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
		p.getInventory().addItem(jetpack());
		p.setLevel(50);
		
		if(p.getKiller() instanceof Player) {

			e.setRespawnLocation(p.getKiller().getLocation());
			
			p.sendMessage(Main.prefix + "You killed by: " + p.getKiller().getName());
			
			p.sendMessage(Main.prefix + "You killed by: " + p.getKiller().getName());
		
		} else {
			
			double x = cfg.getDouble(Main.playerNumber.get(p) + ".x");
			double y = cfg.getDouble(Main.playerNumber.get(p) + ".y");
			double z = cfg.getDouble(Main.playerNumber.get(p) + ".z");
			float yaw = cfg.getInt(Main.playerNumber.get(p) + "yaw");
			float pitch = cfg.getInt(Main.playerNumber.get(p) + "pitch");
			
			Location loc = new Location(Bukkit.getWorld("AirWarriors"), x, y, z);
			
			loc.setYaw(yaw);
			loc.setPitch(pitch);
			
			e.setRespawnLocation(loc);
		}
	}

	private ItemStack jetpack() {
		ItemStack jp = new ItemStack(Material.FLINT_AND_STEEL);
		ItemMeta meta = jp.getItemMeta();
		meta.setDisplayName("Jetpack");
		jp.setItemMeta(meta);
		return jp;
	}
}
