package com.Higgins.aw.Game;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class Jetpack implements Listener {

	@EventHandler
	public void on(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action action = e.getAction();
		
		if(p.getItemInHand().getType() == Material.FLINT_AND_STEEL) {
			
			if(p.getLevel() > 0) {
				
				int level = p.getLevel();
				
				level --;
				
				p.setLevel(level);
				
				if(action == (Action.LEFT_CLICK_AIR) || action == (Action.LEFT_CLICK_BLOCK)) {
					
					Vector v = p.getLocation().getDirection().multiply(2);
					
					v.setY(1);
					
					p.setVelocity(v);
					
					for (Player pl : Bukkit.getOnlinePlayers()) {

						((CraftPlayer) pl).getHandle().playerConnection
								.sendPacket(new PacketPlayOutWorldParticles(EnumParticle.FLAME, true,
										(float) p.getLocation().getX(), (float) p.getLocation().getY(),
										(float) p.getLocation().getZ(), (float) 0.5, (float) 0.7, (float) 0.7, (float) 0, 2));
						
						((CraftPlayer) pl).getHandle().playerConnection
						.sendPacket(new PacketPlayOutWorldParticles(EnumParticle.CLOUD, true,
								(float) p.getLocation().getX(), (float) p.getLocation().getY(),
								(float) p.getLocation().getZ(), (float) 0.5, (float) 0.7, (float) 0.7, (float) 0, 2));
					}
				}
				
				else if(action == (Action.RIGHT_CLICK_AIR) || action == (Action.RIGHT_CLICK_BLOCK)) {
					
					Fireball f = p.launchProjectile(Fireball.class);
					
					Vector v = p.getLocation().getDirection().multiply(3);
					
					f.setShooter(p);
					f.setVelocity(v);
					f.setBounce(false);
					f.setIsIncendiary(false);
				}
				
			} else {
				
				p.sendMessage("�cOh no! �eYou have ran out of fuel!");
			}
		}
	}
}
