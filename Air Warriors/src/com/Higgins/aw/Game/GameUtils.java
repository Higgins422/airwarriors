package com.Higgins.aw.Game;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import com.Higgins.aw.GameState;
import com.Higgins.aw.Main;

public class GameUtils implements Listener {

	@EventHandler
	public void on(BlockBreakEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(BlockPlaceEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			
			Player p = (Player) e.getEntity();
			
			if(GameState.getState(GameState.LOBBY) || e.getCause() == DamageCause.FALL || Main.spectator.contains(p)) {
				
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void on(InventoryClickEvent e) {
		HumanEntity ent = e.getWhoClicked();
		Player p = (Player) ent;
		
		if(Main.spectator.contains(p)) {
			
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void on(WeatherChangeEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(CreatureSpawnEvent e) {
		
		if(!(e.getSpawnReason() == SpawnReason.CUSTOM)) {
			
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void on(FoodLevelChangeEvent e) {
		
		e.setCancelled(true);
	}
}
