package com.Higgins.aw.Game;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.Higgins.aw.Main;

public class Refuel implements Listener {

	@EventHandler
	public void on(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action action = e.getAction();
		
		if(action == (Action.RIGHT_CLICK_BLOCK) || action == (Action.LEFT_CLICK_BLOCK)) {
			
			if(e.getClickedBlock().getType() == Material.COAL_BLOCK) {
				
				int levels = p.getLevel();
				
				p.setLevel(levels + 30);
				
				p.sendMessage(Main.prefix + "+ 30 Fuel!");
			}
		}
	}
}
